# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2] - 2020-10-21

- Fix context instance call by adding _instance classvar

## [0.2.1] - 2020-10-19

- Inherit from zmq.sugar.* for better type hinting

## [0.2.0] - 2020-10-19

- Subclass `zmq.Socket` as `QuickSocket` rather than monkey-patching it.

## [0.1.0] - 2020-10-16

- Initial Release

[Unreleased]: https://gitlab.com/osu-nrsg/zmq-quicklink/-/compare/v0.2.2...master
[0.2.2]: https://gitlab.com/osu-nrsg/zmq-quicklink/-/compare/v0.2.1...0.2.2
[0.2.1]: https://gitlab.com/osu-nrsg/zmq-quicklink/-/compare/v0.2.0...0.2.1
[0.2.0]: https://gitlab.com/osu-nrsg/zmq-quicklink/-/compare/v0.1.0...0.2.0
[0.1.0]: https://gitlab.com/osu-nrsg/zmq-quicklink/-/tags/v0.1.0
